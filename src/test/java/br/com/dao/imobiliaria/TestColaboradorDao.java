package br.com.dao.imobiliaria;

import static org.junit.Assert.assertNotNull;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import br.com.models.imobiliaria.Colaborador;

public class TestColaboradorDao {

	private ColaboradorDao dao = null;
	
	
	@Test
	public void testGetColaboradoresDB(){
		List<Colaborador> colaboradores = dao.getAll();
		Assert.assertFalse(colaboradores.isEmpty());
	}
	
	@Test
	public void testGetOneColaboradorDB(){
		Colaborador colaborador = null;
		colaborador = dao.getOne(10011);
		assertNotNull(colaborador);
	}

	@Before
	public void criaDao() {
		if(this.dao==null)
			this.dao = new ColaboradorDao(Connect.getConn());
	}
}