package br.com.dao.imobiliaria;

import static org.junit.Assert.assertNotNull;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import br.com.models.imobiliaria.Cargo;

public class TestCargoDao {

	@Test
	public void testGetCargosDB(){
		CargoDao dao = new CargoDao(Connect.getConn());
		List<Cargo> cargos = dao.getAll();
		Assert.assertFalse(cargos.isEmpty());
	}
	
	@Test
	public void testGetOneCargoDB(){
		CargoDao dao = new CargoDao(Connect.getConn());
		Cargo cargo = null;
		int id = 3;
		cargo = dao.getOne(id);
		assertNotNull(cargo);
	}
}