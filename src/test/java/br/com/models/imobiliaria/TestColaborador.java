package br.com.models.imobiliaria;

import org.junit.Before;
import org.junit.Test;
import br.com.exceptions.imobiliaria.CriarColaboradorException;

public class TestColaborador {
	
	/*
	 * 	Aplicaremos o TDD na criação de testes unitários de uma nova funcionalidade
	 * para o sistema.
	 * 	O Objetivo é implementar o código que nos irá permitir adcionar um novo
	 *  colaborador de acordo com algumas regras de negócio:
	 *  	1 - Somente um colaborador com cargo Admin pode cadastrar um colaborador
   	 *		2 - um colaborador não pode ser cadastrado sem cargo
	 *		3 - Um colaborador não pode ser chefe de si mesmo
	 *		4 - Um colaborador não pode ter nenhum atributo null ou com valor 0.
	 * 
	 * */
	
	private Colaborador colaboradorADM = null; 
	private Colaborador colaboradorNaoADM = null; 
	
	@Before
	public void iniciaMocks(){
		//Inicialização de dados
		this.colaboradorADM = new Colaborador("Jose", "Bonifacio", "01124205225", 
				"JB1500", 0, "bonifa", "123456", null, new Cargo("Admin", 1));

		this.colaboradorNaoADM = new Colaborador("Joao", "Arribas", "02125588555",
				"JA2002", 0, "arriba", "121212", this.colaboradorADM, new Cargo("Gerente",  2));

	}
	
	/**
	 * Esse teste deve falhar caso a exception CriarColaboradorException
	 * não seja lançada.
	 * 
	 * @throws CriarColaboradorException
	 */
	@Test(expected=CriarColaboradorException.class)
	public void naoAdmCriarColaboradorThrowsExcpetion() 
			throws CriarColaboradorException { 
		colaboradorNaoADM.createColaborador(new Cargo());
	}
	
	
	@Test(expected=CriarColaboradorException.class)
	public void criarColaboradorSemCargoThrowsExcpetion() 
			throws CriarColaboradorException { 
		colaboradorADM.createColaborador(null);
	}
	
}