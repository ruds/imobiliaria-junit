package br.com.models.imobiliaria;

public class Cliente extends Pessoa {
 
	int id;
	
	private String email;
	 
	private Cidade cidade;
	 
	private Cliente indicacao;
	 
	private Aluguel aluguel;
	
	public Cliente(int id, String nome, String sobrenome, String telefone, String registro, int status, String email,
			Cidade cidade, Cliente indicacao, Aluguel aluguel) {
		super(nome, sobrenome, telefone, registro, status);
		this.id = id;
		this.email = email;
		this.cidade = cidade;
		this.indicacao = indicacao;
		this.aluguel = aluguel;
	}

	public Cliente(String nome, String sobrenome, String telefone, String registro, int status, String email,
			Cidade cidade, Cliente indicacao, Aluguel aluguel) {
		super(nome, sobrenome, telefone, registro, status);
		this.email = email;
		this.cidade = cidade;
		this.indicacao = indicacao;
		this.aluguel = aluguel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Cliente getIndicacao() {
		return indicacao;
	}

	public void setIndicacao(Cliente indicacao) {
		this.indicacao = indicacao;
	}

	public Aluguel getAluguel() {
		return aluguel;
	}

	public void setAluguel(Aluguel aluguel) {
		this.aluguel = aluguel;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Cliente [id=" + id + ", email=" + email + ", cidade=" + cidade + ", "
				+ "indicacao=" + indicacao + ", aluguel=" + aluguel + ","
				+ " toString()=" + super.toString() + "]";
	}
	 
}