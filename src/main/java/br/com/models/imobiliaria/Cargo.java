package br.com.models.imobiliaria;

public class Cargo {
 
	private int id;
	private String nomeCargo;
	private double salario;
	
	public Cargo(String nomeCargo, double salario) {
		this.nomeCargo = nomeCargo;
		this.salario = salario;
	}

	public Cargo(int id, String nomeCargo, double salario) {
		this.id = id;
		this.nomeCargo = nomeCargo;
		this.salario = salario;
	}

	public Cargo() {

	}

	public String getNomeCargo() {
		return nomeCargo;
	}

	public void setNomeCargo(String nomeCargo) {
		this.nomeCargo = nomeCargo;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Cargo [id="
				+ id + ", "
				+ (nomeCargo != null ? "nomeCargo=" + nomeCargo + ", " : "")
				+ "salario=" + salario + "]";
	}	 
}