package br.com.models.imobiliaria;

import br.com.exceptions.imobiliaria.CriarColaboradorException;

public class Colaborador extends Pessoa {
 
	private int id;
	
	private String login;
	 
	private String senha;
		 
	private Colaborador chefe;
	 
	private Cargo cargo;

	public Colaborador(int id, String nome, String sobrenome,  String telefone,  
			String regidstro, int status, String login, String senha, Colaborador chefe, Cargo cargo) {
		super(nome, sobrenome, telefone, regidstro, status);
		this.id = id;
		this.login = login;
		this.senha = senha;
		this.chefe = chefe;
		this.cargo = cargo;
	}
	
	public Colaborador(String nome, String sobrenome,  String telefone,  
			String regidstro, int status, String login, String senha, Colaborador chefe, Cargo cargo) {
		super(nome, sobrenome, telefone, regidstro, status);
		this.login = login;
		this.senha = senha;
		this.chefe = chefe;
		this.cargo = cargo;
	}

	public Colaborador() {
		super();
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Colaborador getChefe() {
		return chefe;
	}

	public void setChefe(Colaborador chefe) {
		this.chefe = chefe;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "COLABORADOR "  
				+"["+ (login != null ? "login=" + login + ", " : "")
				+ (senha != null ? "senha=" + senha : "") + "]"+ '\n' 
				+ (cargo != null ? cargo + ", " : "") + '\n'
				+ (super.toString() != null ? super.toString() : "") + '\n'
				+ (chefe != null ? '\n' + "CHEFIA: " + '\n': "")
				+ (chefe != null ? chefe: "");
	}

	public Colaborador createColaborador(Cargo cargo)
			throws CriarColaboradorException{
		
		Colaborador colaborador = new Colaborador();

		verificaAdmin();
		verificaCargo(cargo);
		
		colaborador.setCargo(cargo);
			
		return colaborador;
	}

	private void verificaCargo(Cargo cargo) throws CriarColaboradorException {
		if(cargo==null)
			throw new CriarColaboradorException
			("Um colaborador não pode ser criado ser sem um cargo.");
	}

	private void verificaAdmin() throws CriarColaboradorException {
		if(this.cargo.getId()!=1)
			throw new CriarColaboradorException
			("Somente um colaborador Admin pode adcionar um novo colaborador.");
	}

}