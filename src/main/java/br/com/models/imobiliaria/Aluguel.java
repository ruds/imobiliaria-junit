package br.com.models.imobiliaria;

import java.util.Date;

public class Aluguel {
 
	private int id;
	 
	private Date data;
	 
	private double periodo;
	 
	private double valorDiaria;
	 
	private Colaborador colaborador;
	 
	private Cliente[] cliente;
	 
	private Imovel[] imovel;
	 
	private Anuncio[] anuncio;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public double getPeriodo() {
		return periodo;
	}

	public void setPeriodo(double periodo) {
		this.periodo = periodo;
	}

	public double getValorDiaria() {
		return valorDiaria;
	}

	public void setValorDiaria(double valorDiaria) {
		this.valorDiaria = valorDiaria;
	}

	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public Cliente[] getCliente() {
		return cliente;
	}

	public void setCliente(Cliente[] cliente) {
		this.cliente = cliente;
	}

	public Imovel[] getImovel() {
		return imovel;
	}

	public void setImovel(Imovel[] imovel) {
		this.imovel = imovel;
	}

	public Anuncio[] getAnuncio() {
		return anuncio;
	}

	public void setAnuncio(Anuncio[] anuncio) {
		this.anuncio = anuncio;
	}
	 
}
 
