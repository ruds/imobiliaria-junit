package br.com.models.imobiliaria;

public class Cidade {
 
	private String nome;
	 
	private UF uf;
	 
	private UF uF;
	 
	private Imovel imovel;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public UF getuF() {
		return uF;
	}

	public void setuF(UF uF) {
		this.uF = uF;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}
	 
}
 
