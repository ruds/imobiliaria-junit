package br.com.models.imobiliaria;

public class TipoImovel {
 
	private String id;
	 
	private String nome;
	 
	private Imovel imovel;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}
	 
}
 
