package br.com.models.imobiliaria;

public class Pais {
 
	private String nome;
	 
	private String sigla;
	 
	private UF uF;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public UF getuF() {
		return uF;
	}

	public void setuF(UF uF) {
		this.uF = uF;
	}
	
	 
}
 
