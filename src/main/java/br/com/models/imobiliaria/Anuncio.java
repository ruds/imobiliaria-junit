package br.com.models.imobiliaria;

import java.util.Date;

public class Anuncio {
 
	private String id;
	 
	private Date data;
	 
	private double valorAnunciado;
	 
	private Aluguel aluguel;
	 
	private Imovel[] imovel;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public double getValorAnunciado() {
		return valorAnunciado;
	}

	public void setValorAnunciado(double valorAnunciado) {
		this.valorAnunciado = valorAnunciado;
	}

	public Aluguel getAluguel() {
		return aluguel;
	}

	public void setAluguel(Aluguel aluguel) {
		this.aluguel = aluguel;
	}

	public Imovel[] getImovel() {
		return imovel;
	}

	public void setImovel(Imovel[] imovel) {
		this.imovel = imovel;
	}
	 
}
 
