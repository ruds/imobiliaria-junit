package br.com.models.imobiliaria;

public abstract class Pessoa {
 
	private String nome;
	 
	private String sobrenome;
	 
	private String telefone;
	 
	private String registro;
	 
	private int status;

	public Pessoa(String nome, String sobrenome, String telefone, String registro, int status) {
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.telefone = telefone;
		this.registro = registro;
		this.status = status;
	}

	public Pessoa() {
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public int isStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getRegistro() {
		return registro;
	}

	public void setRegistro(String registro) {
		this.registro = registro;
	}

	@Override
	public String toString() {
		return "Pessoa [" 
				+ (nome != null ? "nome=" + nome + ", " : "")
				+ (sobrenome != null ? "sobrenome=" + sobrenome + ", " : "")
				+ (telefone != null ? "telefone=" + telefone + ", " : "")
				+ (registro != null ? "registro=" + registro + ", " : "")
				+ "status=" + status + "]";
	}
}