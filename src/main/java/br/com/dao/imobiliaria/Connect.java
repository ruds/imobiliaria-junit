package br.com.dao.imobiliaria;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {	
	
	private static Connection myConn = null;
	
	public static Connection getConn() {
		
		if(myConn==null){
			myConn = generateConn();
		}
		return myConn;
	}
	
	private static Connection generateConn(){
		
		Connection conn = null;
		
		try {
			
			DriverManager.registerDriver (new org.postgresql.Driver()); 
			conn = DriverManager.getConnection
				("jdbc:postgresql://localhost:5432/imobiliaria","postgres","1234");
			conn.setAutoCommit(false);
		}
		catch (SQLException e){
			System.out.println ('\n' + "Erro na conexão com o banco.");
			e.printStackTrace();
			System.exit(1);
		}
		
		return conn;
	}
}
