package br.com.dao.imobiliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import br.com.models.imobiliaria.Cargo;
import br.com.models.imobiliaria.Colaborador;

public class ColaboradorDao implements Dao{
	
	private Connection conn = null;
	
	public ColaboradorDao(Connection conn) {
		this.conn = conn;
	}

	public List<Colaborador> getAll() {

		List<Colaborador> colaboradores = null;		
		
		try {	
			
			String query = "SELECT * FROM COLABORADOR AS CB JOIN CARGO"
					+ " AS CG ON CB.IDCARGO = CG.IDCARGO";
			
			PreparedStatement pstmt = conn
				.prepareStatement(query);	
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next()) {
				
				colaboradores = new ArrayList<Colaborador>(40);
			
				do {	
					
					Colaborador colaborador = 
						new Colaborador(
								rs.getString("pnomecol"), 
								rs.getString("snomecol"), rs.getString("telcol"), 
								rs.getString("matcol"), rs.getInt("statuscol"), 
								rs.getString("login"), rs.getString("senha"), 
								this.getOne(rs.getInt("matchefe")), 
								new Cargo(
									rs.getInt("idcargo"),
									rs.getString("nomecargo"),
									rs.getDouble("salario")));
					
					colaboradores.add(colaborador);
					
					
				} while(rs.next());
			}
			rs.close();
			pstmt.close();
		}	
		catch (SQLException e)
		{	e.printStackTrace();
			System.exit(1);
		}
		
		return colaboradores;
		
	}


	public Colaborador getOne(int id) {
		Colaborador colaborador = null;
		try
		{	
			String query = "SELECT * FROM COLABORADOR AS CB JOIN CARGO AS CG "
					+ "ON CB.IDCARGO = CG.IDCARGO WHERE CB.MATCOL  = ?";
			PreparedStatement pstmt = conn
					.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next()){
				colaborador = 
						new Colaborador(
							rs.getString("pnomecol"), 
							rs.getString("snomecol"), rs.getString("telcol"), 
							rs.getString("matcol"), rs.getInt("statuscol"), 
							rs.getString("login"), rs.getString("senha"), 
							this.getOne(rs.getInt("matchefe")), 
							new Cargo(
								rs.getInt("idcargo"),
								rs.getString("nomecargo"),
								rs.getDouble("salario")));
			}
			rs.close();
			pstmt.close();
		}	
		catch (SQLException e)
		{	e.printStackTrace();
			System.exit(1);
		}
		return colaborador;
	}

	@SuppressWarnings("unused")
	private void verifyResultSet(ResultSet rs) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int cols = rsmd.getColumnCount();
		for (int i = 1; i <= cols; i++) {
			System.out.println("NAME: " + rsmd.getColumnName(i) + " - " 
				+ "TYPE: " + rsmd.getColumnTypeName(i) + " - "
				+ "TABLE: " + rsmd.getTableName(i) + ".");
		}
		System.out.println('\n' +"======================================" + '\n');
	}
}