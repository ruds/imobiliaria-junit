package br.com.dao.imobiliaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import br.com.models.imobiliaria.Cargo;

public class CargoDao {

	private Connection conn = null;
	
	public CargoDao(Connection conn) {
		this.conn = conn;
	}

	public List<Cargo> getAll() {

		List<Cargo> arrayCargos = null;		
		
		try
		{	PreparedStatement pstmt = conn
				.prepareStatement("SELECT * FROM CARGO");	
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next())
			{	arrayCargos = new ArrayList<Cargo>(40);
			
				do
				{	arrayCargos.add(new Cargo(rs.getInt("IDCARGO"),
					                                  rs.getString("NOMECARGO"),
						                              rs.getDouble("SALARIO")));
				}
				while(rs.next());
			}
			rs.close();
			pstmt.close();
		}	
		catch (SQLException e)
		{	e.printStackTrace();
			System.exit(1);
		}
		
		return arrayCargos;
		
	}

	public Cargo getOne(int id) {
		
		Cargo umCargo = null;
		try
		{	
			PreparedStatement pstmt = conn
				.prepareStatement("SELECT * FROM CARGO WHERE IDCARGO = ?");
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next())
			{
				umCargo = new Cargo(rs.getInt("IDCARGO"),
                        rs.getString("NOMECARGO"),
                        rs.getDouble("SALARIO"));
			}
			rs.close();
			pstmt.close();
		}	
		catch (SQLException e)
		{	e.printStackTrace();
			System.exit(1);
		}
		return umCargo;
	}
}