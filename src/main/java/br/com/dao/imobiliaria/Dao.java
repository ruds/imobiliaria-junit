package br.com.dao.imobiliaria;

import java.util.List;

public interface Dao {
	
	public List<?>  getAll();
	
	public Object getOne(int id);

}