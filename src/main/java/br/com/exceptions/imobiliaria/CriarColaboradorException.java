package br.com.exceptions.imobiliaria;

public class CriarColaboradorException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public CriarColaboradorException(String msg) {
		super(msg);
	}
}
